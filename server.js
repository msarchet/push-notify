var url = require('url'),
		util = require('util');

var port = process.env.port || 8001;

var app = require('http').createServer(handler)
  , io = require('socket.io').listen(app)

var sockets = [];

app.listen(port);
util.log('socket.io server started on port: ' + port + '\n');

function handler (req, res) {
	var path = url.parse(req.url, true).pathname;
	if(path === '/') {
					res.writeHead(200);
					res.end('socket.io server started on port: ' + port + '\n');
	} else if( path === '/pr') {
		if(req.method === 'POST') {
						var postData = "";
						req.on('data', function(chunk) { 
							postData += chunk;	
						});

						req.on('end', function() { 
							var data = JSON.parse(postData);
							sockets.forEach(function(s) {
								s.emit('notify', data);	 
							})
							res.writeHead(204);
							res.end();
						});
		}
	}
}

io.sockets.on('connection', function (socket) {
  util.log('user connected');
	sockets.push(socket);
});



