var notifier = require('./node_modules/grunt-notify/lib/notify-lib'),
    client = require('socket.io-client'),
		http = require('http'),
		url = require('url');

var createNotification = function(message) { 
  notifier({message : message, title : 'TOAST'});
};

var prs = [];
var notify = true;

var app = http.createServer(function(req, res) {
	res.writeHead(200, {'content-type': 'text/html'});
	res.write('<h1>Pull Requests</h1>');
	res.write('<ul>');	
	prs.forEach(function(p) {
		res.write('<li>' + p.message + '</li>'); 
	});
	res.write('</ul>');
	res.end();
}).listen(8002);


var sockets = client.connect('http:localhost:8001');
sockets.on('notify', function(data) { 
				prs.push(data);	

				if(notify) { 
								createNotification(data.message);
				}
});

